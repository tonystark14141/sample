import { Component, OnInit } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { NavController } from '@ionic/angular';
import { Router } from '@angular/router';
import { ProSerService } from "../../services/pro-ser.service";
@Component({
  selector: 'app-view-profile',
  templateUrl: './view-profile.page.html',
  styleUrls: ['./view-profile.page.scss'],
})
export class ViewProfilePage implements OnInit {
list:any;
data:any;

  constructor(public ser:ProSerService, public http:HttpClient, public router:Router
  	, public cntrl:NavController) { }
get_profile()
{
		this.ser.profilelistall().subscribe(params=>{
  		console.log(params);
  		this.list=params.data;
  		console.log("my data", this.list);
  	})
}
del_profile(id)
{
 this.ser.profiledelete(id).subscribe(res=>{
  this.data=res;
  this.get_profile();
});
 
}
  ngOnInit() {
  	this.get_profile();
  }

}
