import { Component, OnInit } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { NavController } from '@ionic/angular';
import { Router, ActivatedRoute } from '@angular/router';
import { ProSerService } from "../../services/pro-ser.service";
import { ToastController } from '@ionic/angular';
@Component({
  selector: 'app-update-profile',
  templateUrl: './update-profile.page.html',
  styleUrls: ['./update-profile.page.scss'],
})
export class UpdateProfilePage implements OnInit {
f_name:any;
l_name:any;
address:any;
m_num:any;
id:any;
prodata:Object;
finaldata:any;
	view_pro={
		id:"",
		f_name:"",
		l_name:"",
		address:"",
		m_num:"",
	}

  constructor(public ser:ProSerService, public http:HttpClient, 
  	public router:Router,public toastController:ToastController
  	, public cntrl:NavController,public route:ActivatedRoute) { }
  async toaster(message){  //message is only one common parameter for using commands "plz enter cus_Name"

  const toast = await this.toastController.create({  //LOGIN SUCCESS CODE IN TOAST
      message:message,
      duration: 1000,
      color:'success',
      position: 'middle',
    });
  await toast.present();
}
async toaster1(message){  //message is only one common parameter for using commands "plz enter cus_Name"

  const toast = await this.toastController.create({  //LOGIN SUCCESS CODE IN TOAST
      message:message,
      duration: 3000,
      color:'danger',
      showCloseButton: true,
      closeButtonText: 'OK',
      position: 'middle',
    });
  await toast.present();
}
updateprofile()
{
	this.prodata={
		id:this.id,
		f_name:this.f_name,
		l_name:this.l_name,
		address:this.address,
		m_num:this.m_num,
	}
	console.log("pro", this.prodata);
	this.ser.profileupdate(this.id,this.prodata).subscribe(res=>{

console.log("kycinserdata",res);
this.finaldata=res;
console.log(this.finaldata)

	if(res.code==200)
{
	this.toaster(' Data inserted success')
  // this.router.navigate(["/view-profile"]);
}
else{
	this.toaster1('Data inserted failed')
}
});
}
pro_list(id)
{

	this.ser.profilelist(id).subscribe(params=>{
  		// console.log(params);
  		this.view_pro=params.data[0];
  		console.log("my data", this.view_pro);
  	})
}
pro_id()
{
this.route.params.subscribe(data=>{
  		this.id=data['pid'];
  		console.log(this.id);
  	});	
}
  ngOnInit() {
  	 	this.pro_id();
  	  	this.pro_list(this.id);
  }

}
