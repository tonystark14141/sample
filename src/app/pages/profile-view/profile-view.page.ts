import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { HttpClient } from  '@angular/common/http';
import { ProSerService } from "../../services/pro-ser.service";

@Component({
  selector: 'app-profile-view',
  templateUrl: './profile-view.page.html',
  styleUrls: ['./profile-view.page.scss'],
})
export class ProfileViewPage implements OnInit {
view_pro:Object;
id:any;
data:any;
  constructor(public route:ActivatedRoute, public http:HttpClient,
   public ser:ProSerService) { }
pro_list(id)
{
	this.ser.profilelist(id).subscribe(params=>{
  		console.log(params);
  		this.view_pro=params.data[0];
  		console.log("my data", this.view_pro);
  	})
}
pro_id()
{
this.route.params.subscribe(data=>{
  		this.id=data['pid'];
  		console.log(this.id);
  	});	
}
  ngOnInit() {
  	this.pro_id();
  	  	this.pro_list(this.id);

  }

}
