import { Component, OnInit } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { NavController } from '@ionic/angular';
import { Router } from '@angular/router';
import { ProSerService } from "../../services/pro-ser.service";
import { ToastController } from '@ionic/angular';

@Component({
  selector: 'app-add-profile',
  templateUrl: './add-profile.page.html',
  styleUrls: ['./add-profile.page.scss'],
})
export class AddProfilePage implements OnInit {
	// id:any;
f_name:any;
l_name:any;
address:any;
m_num:any;
prodata:Object;
finaldata:any;
  constructor(public ser:ProSerService, public http:HttpClient, 
  	public router:Router,public toastController:ToastController
  	, public cntrl:NavController) { }
  async toaster(message){  //message is only one common parameter for using commands "plz enter cus_Name"

  const toast = await this.toastController.create({  //LOGIN SUCCESS CODE IN TOAST
      message:message,
      duration: 1000,
      color:'success',
      position: 'middle',
    });
  await toast.present();
}
async toaster1(message){  //message is only one common parameter for using commands "plz enter cus_Name"

  const toast = await this.toastController.create({  //LOGIN SUCCESS CODE IN TOAST
      message:message,
      duration: 3000,
      color:'danger',
      showCloseButton: true,
      closeButtonText: 'OK',
      position: 'middle',
    });
  await toast.present();
}

addprofile()
{
	this.prodata={
		// id:this.id,
		f_name:this.f_name,
		l_name:this.l_name,
		address:this.address,
		m_num:this.m_num,
	}
	this.ser.profileadd(this.prodata).subscribe(res=>{

//console.log("kycinserdata",res);
this.finaldata=res;
console.log(this.finaldata)

	if(res.code==200)
{
	this.toaster(' Data inserted success')
  this.router.navigate(["/view-profile"]);
}
else{
	this.toaster1('Data inserted failed')
}
});
}

  ngOnInit() {
  }

}
