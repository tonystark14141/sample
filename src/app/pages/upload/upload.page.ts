import { Component, OnInit } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { NavController } from '@ionic/angular';
import { Router } from '@angular/router';
import { ProSerService } from "../../services/pro-ser.service";
import { ToastController } from '@ionic/angular';

@Component({
  selector: 'app-upload',
  templateUrl: './upload.page.html',
  styleUrls: ['./upload.page.scss'],
})
export class UploadPage implements OnInit {

  constructor(public ser:ProSerService, public http:HttpClient, 
  	public router:Router,public toastController:ToastController
  	, public cntrl:NavController) { }


finaldata:any;
filename: string = '' ;
onFileSelected(event){
  let file = event.target.files[0];        
  this.filename= file.name;
  console.log(this.filename);
}
upload()
{
this.ser.fileupload(this.filename).subscribe(res=>{

//console.log("kycinserdata",res);
this.finaldata=res;
console.log(this.finaldata)
});
}


  ngOnInit() {
  }

}
