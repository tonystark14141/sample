import { Injectable } from '@angular/core';
import {HttpClient,HttpHeaders,HttpErrorResponse} from '@angular/common/http';
import {Observable , throwError} from 'rxjs';
import{retry , catchError} from 'rxjs/operators';
// import { Constants } from '../constant/constants';

@Injectable({
  providedIn: 'root'
})
export class ProSerService {
basepath="http://localhost/api_ser1/index.php/"
basepath1="http://localhost/fileupload-api/"
  constructor(private http:HttpClient) { }
   httpOptions={
	headers:new HttpHeaders({'content-type' : 'applicaton/json'})

}
profilelist(invoice):Observable<any>{
	return this.http.get(this.basepath + 'profile_list?id=' + invoice).pipe(retry(2))
}
profilelistall():Observable<any>{
	return this.http.get(this.basepath + 'profile_list?id=-1').pipe(retry(2))
}
 profiledelete(id):Observable<any>{
   return this.http.delete(this.basepath +'delete_profile/'+id).pipe(retry(2));
 }
profileadd(userItem):Observable<any>{
	return this.http.post(this.basepath +'add_profile' ,JSON.stringify(userItem),
		this.httpOptions).pipe(retry(2))
}
 profileupdate(id,item):Observable<any>{
return this.http.put(this.basepath + 'update_profile' , JSON.stringify(item),this.httpOptions).pipe(retry(2))
}
fileupload(file):Observable<any>{
	console.log(file);
	return this.http.post(this.basepath1 +'index_image.php' ,JSON.stringify(file),this.httpOptions).pipe(retry(2))
} 
}
