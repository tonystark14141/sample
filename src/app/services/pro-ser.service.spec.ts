import { TestBed } from '@angular/core/testing';

import { ProSerService } from './pro-ser.service';

describe('ProSerService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: ProSerService = TestBed.get(ProSerService);
    expect(service).toBeTruthy();
  });
});
